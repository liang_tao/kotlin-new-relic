val deps: Map<String, String> by extra

dependencies {
    api("com.newrelic.agent.java", "newrelic-api", "5.14.0")
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.3.9")
    implementation("org.slf4j", "slf4j-api", deps["slf4j"])
}

group = "org.mpierce.kotlin.coroutines.newrelic"
