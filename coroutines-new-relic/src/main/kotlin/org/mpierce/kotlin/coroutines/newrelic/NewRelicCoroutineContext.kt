package org.mpierce.kotlin.coroutines.newrelic

import com.newrelic.api.agent.Segment
import com.newrelic.api.agent.Token
import com.newrelic.api.agent.Trace
import com.newrelic.api.agent.Transaction
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

private val logger = LoggerFactory.getLogger("org.mpierce.kotlin.coroutine.newrelic")

/**
 * Coroutine context for a New Relic Transaction
 */
class NewRelicTransaction(val txn: Transaction) :
    AbstractCoroutineContextElement(NewRelicTransaction) {
    companion object Key : CoroutineContext.Key<NewRelicTransaction>
}

/**
 * Coroutine context for a New Relic Segment
 */
class NewRelicSegment(val segment: Segment) :
    AbstractCoroutineContextElement(NewRelicSegment) {
    companion object Key : CoroutineContext.Key<NewRelicSegment>
}

/**
 * Coroutine context for a New Relic Token
 */
class NewRelicToken(val token: Token) : AbstractCoroutineContextElement(NewRelicToken) {
    companion object Key : CoroutineContext.Key<NewRelicToken>
}

/**
 * If a NR transaction is available in the coroutine context, this starts a segment with the given name
 * and adds it to the coroutine context as a [NewRelicSegment] element, runs the block in that context, and then
 * ends the segment.
 *
 * If a transaction is not available in the coroutine context, the block is run in the current coroutine context.
 *
 * When running in a separate thread from the original transaction, this needs to be run inside the scope of a Token.
 * See [withNewRelicToken].
 *
 * See https://docs.newrelic.com/docs/agents/java-agent/async-instrumentation/java-agent-api-asynchronous-applications for more on segments and tokens.
 */
suspend fun <T> withNewRelicSegment(name: String, block: suspend () -> T): T {
    val context = coroutineContext[NewRelicTransaction]?.let {
        NewRelicSegment(it.txn.startSegment(name))
    }
    return if (context != null) {
        withContext(context) {
            try {
                block()
            } finally {
                context.segment.end()
            }
        }
    } else {
        block()
    }
}

/**
 * Run `block` in `dispatcher` with a NR Token applied for the lifetime of the block. The token is created from the
 * calling coroutine's [NewRelicTransaction] context element, or if a NR Transaction is not available in the context,
 * the block is run in the dispatcher without otherwise changing the context.
 *
 * Tokens are how New Relic links work across threads into one transaction. See https://docs.newrelic.com/docs/agents/java-agent/async-instrumentation/java-agent-api-asynchronous-applications
 * for more on tokens.
 *
 */
suspend fun <T> withNewRelicToken(dispatcher: CoroutineDispatcher, block: suspend () -> T): T {
    // Getting the token from a transaction has to be done on the thread that the token belongs to, so
    // we have to get the token, switch dispatchers, then link the token, rather than just accessing
    // the transaction out of the context once the dispatcher has been switched
    val context = coroutineContext[NewRelicTransaction]?.let {
        val token = it.txn.token
        if (!token.isActive) {
            logger.debug("New token is inactive - no transaction on this thread?")
            null
        } else {
            NewRelicToken(token)
        }
    }
    return if (context != null) {
        withContext(context + dispatcher) {
            try {
                runInAsyncTrace {
                    if (!context.token.link()) {
                        logger.debug("Token link failed")
                    }
                    block()
                }
            } finally {
                if (!context.token.expire()) {
                    logger.debug("Token expiration failed")
                }
            }
        }
    } else {
        withContext(dispatcher) {
            block()
        }
    }
}

/**
 * Need to have a function with `async = true` for NR agent to trace work w/ token and link it to the parent txn
 */
@Trace(async = true)
private suspend fun <T> runInAsyncTrace(block: suspend () -> T): T = block()
