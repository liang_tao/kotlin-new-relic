 [ ![Download](https://api.bintray.com/packages/marshallpierce/maven/coroutines-new-relic/images/download.svg) ](https://bintray.com/marshallpierce/maven/coroutines-new-relic/_latestVersion) 

# Kotlin Coroutines and New Relic instrumentation

In your `build.gradle`, add the `jcenter()` repository and:
 
```
implementation("org.mpierce.kotlin.coroutines.newrelic:coroutines-new-relic:LATEST_VERSION")
```

There are three New Relic concepts exposed here: Transactions, Tokens, and Segments, and their corresponding coroutine context elements in this library: `NewRelicTransaction`, `NewRelicSegment`, and `NewRelicToken`.

Transactions are a top level concept that you might use to instrument the server side logic that handles a single web request, for instance. A Transaction can have multiple Segments to represent different parts of the overall execution time. Since Transactions are effectively thread local, Tokens are used to attach work done in other threads to the parent Transaction.

## Transactions

Transactions are created via the [New Relic Agent API](https://docs.newrelic.com/docs/agents/java-agent/async-instrumentation/java-agent-api-asynchronous-applications#tokens_segments) ([javadoc](http://newrelic.github.io/java-agent-api/javadoc/index.html?com/newrelic/api/agent/NewRelic.html)). Typically, transactions would be created by some sort of filter or other integration, as in [ktor-new-relic](../ktor-new-relic).
 
 Once you have a transaction (which has to be done inside a method annotated with `@Trace`; see the New Relic Agent API docs), it can be stashed in the coroutine context with `NewRelicTransaction`. This you would need to do yourself:
 
```kotlin
// helper to run code inside @Trace so we can start a transaction
@Trace(dispatcher = true)
private suspend fun callInTxn(block: suspend () -> Unit) {
    block()
}
 
// ...

fun runSomeBusinessLogic() {
    callInTxn {
        val txn = com.newrelic.api.agent.NewRelic.getAgent().transaction
        // configure transaction as needed
        
        withContext(NewRelicTransaction(txn)) {
            // run business logic that should be in a transaction
        }
    }
}
```

## Segments

Segments are used to delineate sections of code you might want to account for separately.

If you have some code that does two things:

```kotlin
suspend fun doStuff() {
    thing1()
    thing2()
}
```

... you might want to know the time taken for each thing using the `withNewRelicSegment` function. When called with `NewRelicTransaction` present in the coroutine context, it runs the provided block and also exposes the segment via the coroutine context in case you need to manipulate the segment (which is unlikely):

```kotlin
suspend fun doStuff() {
    withNewRelicSegment("thing1") {
        thing1()
    }
    withNewRelicSegment("thing2") {
        thing2()
    }
}
```

Now, those segments will show up in the New Relic UI separately.

## Tokens

Since Transactions are thread local, passing work off to another thread leads to that work not being accounted for at all:

```kotlin
withContext(Dispatchers.IO) {
    // won't be tracked, unless you're already on the same thread that would be used here
}
```

You can use a Token (done for you under the hood by `withNewRelicToken`) to attach work done in another thread to a transaction (which must be available in the coroutine context):

```kotlin
withNewRelicToken(Dispatchers.IO) {
    // Will be attached to the transaction.
    // You an also use withNewRelicSegment here
}
```
