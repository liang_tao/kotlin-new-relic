# Demo of Ktor integration

- [Download the new relic zip](https://docs.newrelic.com/docs/agents/java-agent/installation/install-java-agent) containing the agent jar and the default config file
- Edit the config file to have your license key
- Start up a postgres container with docker compose
    - `docker-compose up -d`
- Package the demo
    - `./gradlew :ktor-demo:distTar`
- Untar the resulting `ktor-demo/build/distributions/ktor-demo.tar` archive somewhere convenient
- Run the `ktor-demo` script that you just untarred with the NR agent
    - `JAVA_OPTS="-javaagent:path/to/newrelic.jar" path/to/bin/ktor-demo`
- Or, run `Demo` from the IDE with equivalent config.
- Hit `http://localhost:8080/delay`, etc. See `KtorDemoMain` for other available endpoints that exercise ways of using coroutines.

Shell snippet to hit all the endpoints for the lazy (zsh array syntax):

```
for p (intentional404 delay delayInSegment delayInSegments delayInSegmentInAnotherThread delayInSegmentInAnotherThreadWithToken delayInAnotherThread slowQuery slowQueryIoDispatcher slowQueryIoDispatcherToken slowQueryAnotherThreadDispatcher slowQueryAnotherThreadDispatcherToken kaboom); do
echo "--> $p"
curl -w '\ntime_total:  %{time_total}\n' localhost:20001/$p
done
```
