plugins {
    application
}

val deps: Map<String, String> by extra

dependencies {
    implementation(project(":ktor-new-relic"))
    implementation(project(":coroutines-new-relic"))
    implementation("io.ktor", "ktor-server-netty", deps["ktor"])

    runtimeOnly("ch.qos.logback", "logback-classic", "1.2.3")

    implementation("com.zaxxer", "HikariCP", "3.4.5")
    runtimeOnly("org.postgresql", "postgresql", "42.2.15")
}

application {
    mainClassName = "org.mpierce.ktor.newrelic.KtorDemoMain"
}
