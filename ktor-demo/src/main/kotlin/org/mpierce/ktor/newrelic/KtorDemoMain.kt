package org.mpierce.ktor.newrelic

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.mpierce.kotlin.coroutines.newrelic.withNewRelicSegment
import org.mpierce.kotlin.coroutines.newrelic.withNewRelicToken
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors

val anotherThreadDispatcher = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

object KtorDemoMain {
    @JvmStatic
    fun main(args: Array<String>) {
        // Sigh, new relic makes startup so slow. Do db init in a separate thread
        val dsFuture = CompletableFuture.supplyAsync {
            HikariConfig().apply {
                jdbcUrl = "jdbc:postgresql://localhost:20000/ktor-newrelic-demo"
                username = "ktor-newrelic-demo"
                password = "ktor-newrelic-demo"
                isAutoCommit = false
            }.let { HikariDataSource(it) }
        }

        // A dummy sql query to see if NR can detect it
        fun slowQuery() {
            dsFuture.get().connection.use { conn ->
                // execute a query that returns a row to hint to new relic that it's a "real" query
                conn.prepareStatement("SELECT 1 from pg_sleep(0.5)").use { stmt ->
                    stmt.execute()
                }
                conn.commit()
            }
        }

        val server = embeddedServer(Netty, port = 20001) {
            install(Routing) {
                route("/") {
                    setUpNewRelic()

                    get("/delay") {
                        // time not allocated to anything
                        delay(500)
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/delayInSegment") {
                        // time is correctly allocated to the segment
                        withNewRelicSegment("delay") {
                            delay(500)
                        }
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/delayInSegments") {
                        // time allocated to each segment, but it's treated as a total (1200ms) which is misleading
                        coroutineScope {
                            launch {
                                withNewRelicSegment("delay1") {
                                    delay(500)
                                }
                            }
                            launch {
                                withNewRelicSegment("delay2") {
                                    delay(700)
                                }
                            }
                        }
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/delayInSegmentInAnotherThread") {
                        // time not allocated to segment
                        withContext(anotherThreadDispatcher) {
                            withNewRelicSegment("anotherThread") {
                                delay(500)
                            }
                        }
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/delayInSegmentInAnotherThreadWithToken") {
                        // correctly allocated
                        withNewRelicToken(anotherThreadDispatcher) {
                            withNewRelicSegment("anotherThread") {
                                delay(500)
                            }
                        }
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/delayInAnotherThread") {
                        // time not allocated to anything
                        withContext(anotherThreadDispatcher) {
                            delay(500)
                        }
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/slowQuery") {
                        // time allocated to query (fixed as of NR 5.14)
                        slowQuery()
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/slowQueryIoDispatcher") {
                        // time not allocated to anything
                        withContext(Dispatchers.IO) {
                            slowQuery()
                        }
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/slowQueryIoDispatcherToken") {
                        // correctly allocated to query
                        withNewRelicToken(Dispatchers.IO) {
                            slowQuery()
                        }
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/slowQueryAnotherThreadDispatcher") {
                        // time not allocated to anything
                        withContext(anotherThreadDispatcher) {
                            slowQuery()
                        }
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/slowQueryAnotherThreadDispatcherToken") {
                        // correctly allocated to query
                        withNewRelicToken(anotherThreadDispatcher) {
                            slowQuery()
                        }
                        call.respond(HttpStatusCode.OK, "done")
                    }
                    get("/kaboom") {
                        call.respond(HttpStatusCode.InternalServerError)
                    }
                }
            }
        }
        server.start(wait = true)
    }
}
