 [ ![Download](https://api.bintray.com/packages/marshallpierce/maven/coroutines-new-relic/images/download.svg) ](https://bintray.com/marshallpierce/maven/coroutines-new-relic/_latestVersion) 

# Coroutine integration

See the [`coroutines-new-relic`](coroutines-new-relic) subproject for coroutine context elements for Transactions and Segments. These are used in [`ktor-new-relic`](ktor-new-relic), but are also suitable for any other coroutine-based system.

# Ktor integration

See the [`ktor-new-relic`](ktor-new-relic) subproject for a Ktor feature that wraps each call in a NR Transaction, and the [`ktor-demo`](ktor-demo) subproject to see it in action.

