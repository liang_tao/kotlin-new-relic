val deps: Map<String, String> by extra

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(project(":coroutines-new-relic"))

    implementation("io.ktor:ktor-server-core:${deps["ktor"]}")
    implementation("io.ktor:ktor-jackson:${deps["ktor"]}")
    testImplementation("io.ktor:ktor-server-test-host:${deps["ktor"]}")

    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

group = "org.mpierce.ktor.newrelic"
