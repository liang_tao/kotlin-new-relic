package org.mpierce.ktor.newrelic

import com.newrelic.api.agent.Transaction
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.RoutingApplicationCall
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

// At the moment all we can really do is test that it doesn't crash since the agent won't be live, and even
// If it was, how could we know it worked? New Relic updates very slowly, so it's not like we could hit
// their API.
internal class KtorNewRelicTest {
    @Test
    internal fun featureDoesntCrash() {
        val appInit: Application.() -> Unit = {
            install(NewRelic)
            routing {
                get("/foo") {
                    call.respond(HttpStatusCode.OK)
                }
            }
        }

        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/foo?bar=baz")) {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    internal fun featureWithConfiguredNamerDoesntCrash() {
        val appInit: Application.() -> Unit = {
            install(NewRelic) {
                namer(RequestUriNamer)
            }
            routing {
                get("/foo") {
                    call.respond(HttpStatusCode.OK)
                }
            }
        }

        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/foo?bar=baz")) {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    internal fun routeInterceptorDoesntCrash() {
        val appInit: Application.() -> Unit = {
            routing {
                route("/") {
                    setUpNewRelic()

                    get("/foo") {
                        call.respond(HttpStatusCode.OK)
                    }
                }
            }
        }

        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/foo?bar=baz")) {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    internal fun routeInterceptorGetsRouteCall() {
        val appInit: Application.() -> Unit = {
            routing {
                route("/") {
                    intercept(ApplicationCallPipeline.Monitoring) {
                        require(call is RoutingApplicationCall)
                    }

                    get("/foo") {
                        call.respond(HttpStatusCode.OK)
                    }
                }
            }
        }

        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/foo?bar=baz")) {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    internal fun namerGetsCalledWithDefaultPredicate() {
        val namer = CountingNamer(RequestUriNamer)
        val appInit: Application.() -> Unit = {
            routing {
                route("/") {
                    setUpNewRelic {
                        namer(namer)
                    }

                    get("/foo") {
                        call.respond(HttpStatusCode.OK)
                    }
                }
            }
        }

        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/foo?bar=baz")) {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }

        assertEquals(1, namer.count)
    }

    @Test
    internal fun namerNotCalledWithDenyPredicate() {
        val namer = CountingNamer(RequestUriNamer)
        val appInit: Application.() -> Unit = {
            routing {
                route("/") {
                    setUpNewRelic {
                        namer(namer)
                        predicate {
                            when (it) {
                                // deny /foo/{stuff} route
                                is RoutingApplicationCall -> {
                                    it.route.toString() != "/foo/{stuff}/(method:GET)"
                                }
                                // if somehow we end up with a plain call, just allow it
                                else -> true
                            }
                        }
                    }

                    get("/foo/{stuff}") {
                        call.respond(HttpStatusCode.OK)
                    }
                }
            }
        }

        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/foo/quux?bar=baz")) {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }

        assertEquals(0, namer.count)
    }
}

class CountingNamer(private val delegate: TransactionNamer) : TransactionNamer {
    @Volatile
    var count = 0

    override fun setTxnName(call: ApplicationCall, txn: Transaction) {
        count += 1
        delegate.setTxnName(call, txn)
    }
}
