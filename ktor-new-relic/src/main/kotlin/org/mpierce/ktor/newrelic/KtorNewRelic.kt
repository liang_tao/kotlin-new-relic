package org.mpierce.ktor.newrelic

import com.newrelic.api.agent.ExtendedRequest
import com.newrelic.api.agent.ExtendedResponse
import com.newrelic.api.agent.HeaderType
import com.newrelic.api.agent.Trace
import com.newrelic.api.agent.Transaction
import com.newrelic.api.agent.TransactionNamePriority
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.ApplicationFeature
import io.ktor.application.call
import io.ktor.request.ApplicationRequest
import io.ktor.request.header
import io.ktor.request.httpMethod
import io.ktor.request.uri
import io.ktor.response.ApplicationResponse
import io.ktor.routing.Route
import io.ktor.routing.RoutingApplicationCall
import io.ktor.util.AttributeKey
import io.ktor.util.pipeline.PipelineContext
import kotlinx.coroutines.withContext
import org.mpierce.kotlin.coroutines.newrelic.NewRelicTransaction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.Enumeration
import java.util.NoSuchElementException

/**
 * New Relic integration for Ktor.
 *
 * Wraps each request in a New Relic Transaction, which is available in the request's coroutine context via
 * [NewRelicTransaction].
 *
 *  `install()` it in the ktor app initialization, and configure a custom `TransactionNamer` if desired.
 */
class NewRelic(
    private val namer: TransactionNamer,
    private val predicate: (ApplicationCall) -> Boolean
) {
    class Configuration(
        internal var namer: TransactionNamer,
        internal var predicate: (ApplicationCall) -> Boolean
    ) {
        /**
         * Set the transaction namer to use.
         */
        fun namer(namer: TransactionNamer) {
            this.namer = namer
        }

        /**
         * Set the predicate used to determine if a call should be wrapped in a NR Transaction.
         *
         * If the provided function evaluates to true, a Transaction will be created, otherwise the call will proceed
         * as if this feature was not present.
         */
        fun predicate(predicate: (ApplicationCall) -> Boolean) {
            this.predicate = predicate
        }
    }

    companion object Feature : ApplicationFeature<Application, Configuration, NewRelic> {
        override val key: AttributeKey<NewRelic> = AttributeKey("New Relic integration")

        override fun install(pipeline: Application, configure: Configuration.() -> Unit): NewRelic {
            val config = Configuration(RequestUriNamer) { true }.apply(configure)
            val feature = NewRelic(config.namer, config.predicate)

            pipeline.intercept(ApplicationCallPipeline.Monitoring) {
                runPipelineInTransaction(feature.namer, feature.predicate)
            }

            return feature
        }
    }
}

/**
 * Set up New Relic transactions using routes for transaction names, or a custom `TransactionNamer`.
 *
 * This must be called inside a route declaration. Typically, this would be at the root:
 *
 * ```
 * install(Routing) {
 *     route("/") {
 *         setUpNewRelic() {
 *             // optionally customize the namer if the default route-based namer isn't to your liking
 *             namer(YourCustomNamer())
 *         }
 *     // other routes, etc...
 *     }
 * }
 * ```
 */
fun Route.setUpNewRelic(configure: NewRelic.Configuration.() -> Unit = {}) {
    val config = NewRelic.Configuration(RouteNamer()) { true }.apply(configure)

    val namer = config.namer
    val predicate = config.predicate

    intercept(ApplicationCallPipeline.Monitoring) {
        runPipelineInTransaction(namer, predicate)
    }
}

interface TransactionNamer {
    /**
     * Set the name on the transaction based on the call.
     */
    fun setTxnName(call: ApplicationCall, txn: Transaction)
}

/**
 * Set transaction name based on the matched route.
 *
 * See https://ktor.io/servers/features/routing.html for more on routing.
 *
 * Only works when installed as an interceptor on a route. See [Route.setUpNewRelic] to do this.
 */
class RouteNamer : TransactionNamer {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(RouteNamer::class.java)
    }

    @Volatile
    private var nonRouteCallSeen = false

    override fun setTxnName(call: ApplicationCall, txn: Transaction) {
        if (call is RoutingApplicationCall) {
            // https://newrelic.github.io/java-agent-api/javadoc/com/newrelic/api/agent/Transaction.html#setTransactionName-com.newrelic.api.agent.TransactionNamePriority-boolean-java.lang.String-java.lang.String...-
            txn.setTransactionName(
                TransactionNamePriority.FRAMEWORK_HIGH,
                false,
                "ktor",
                call.route.toString()
            )
        } else {
            if (!nonRouteCallSeen) {
                nonRouteCallSeen = true
                // only log once
                logger.warn("Got a non-Route call. Ktor / New Relic integration may be installed incorrectly.")
            }
        }
    }
}

/**
 * Set transaction name based on the incoming request URI.
 *
 * This uses the literal URI, not the route, so it `/users/alice` and `/users/bob` will be named
 * exactly as shown, not like the presumably matching route `/users/{name}`.
 */
object RequestUriNamer : TransactionNamer {
    override fun setTxnName(call: ApplicationCall, txn: Transaction) {
        txn.setTransactionName(
            TransactionNamePriority.FRAMEWORK_HIGH,
            false,
            "ktor",
            call.request.uri
        )
    }
}

private suspend fun PipelineContext<Unit, ApplicationCall>.runPipelineInTransaction(
    namer: TransactionNamer,
    predicate: (ApplicationCall) -> Boolean
) {
    if (predicate(call)) {
        callInTxn {
            val txn = com.newrelic.api.agent.NewRelic.getAgent().transaction
            txn.setWebRequest(KtorRequest(call.request))
            txn.setWebResponse(KtorResponse(call.response))

            namer.setTxnName(call, txn)

            withContext(NewRelicTransaction(txn)) {
                proceed()
            }
        }
    } else {
        proceed()
    }
}

private class KtorRequest(private val request: ApplicationRequest) : ExtendedRequest() {
    override fun getHeaderType(): HeaderType = HeaderType.HTTP

    override fun getRemoteUser(): String? {
        // TODO we want to run before authentication; how could we have the user callInTxn this?
        return null
    }

    override fun getParameterNames(): Enumeration<*> {
        // TODO -- what exactly is considered a parameter?
        return EmptyEnumeration<Any>()
    }

    override fun getParameterValues(name: String?): Array<String>? {
        return null
    }

    override fun getAttribute(name: String?): Any? {
        // servlet specific, not applicable
        return null
    }

    override fun getCookieValue(name: String): String? = request.cookies[name]

    override fun getMethod(): String = request.httpMethod.value

    override fun getHeader(name: String): String? = request.header(name)

    override fun getRequestURI(): String = request.uri
}

private class KtorResponse(private val response: ApplicationResponse) : ExtendedResponse() {
    override fun getHeaderType(): HeaderType = HeaderType.HTTP

    override fun getStatus(): Int = response.status()?.value ?: 0

    override fun getStatusMessage(): String? = null

    override fun setHeader(name: String, value: String) = response.headers.append(name, value)

    override fun getContentLength(): Long = response.headers["Content-Length"]?.toLongOrNull() ?: 0L

    override fun getContentType(): String? = response.headers["Content-Type"]
}

private class EmptyEnumeration<T> : Enumeration<T> {
    override fun hasMoreElements(): Boolean = false
    override fun nextElement(): T = throw NoSuchElementException()
}

// have to be able to annotate a method with @Trace, so we make a method...
@Trace(dispatcher = true)
private suspend fun callInTxn(block: suspend () -> Unit) {
    block()
}
