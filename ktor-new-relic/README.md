 [ ![Download](https://api.bintray.com/packages/marshallpierce/maven/ktor-new-relic/images/download.svg) ](https://bintray.com/marshallpierce/maven/ktor-new-relic/_latestVersion) 

# Ktor & New Relic

In your `build.gradle`, add the `jcenter()` repository and:
 
```
implementation("org.mpierce.ktor.newrelic:ktor-new-relic:LATEST_VERSION")
```

This lib provides New Relic Transactions for each request to a [Ktor](https://ktor.io/) app using features from [coroutines-new-relic](../coroutines-new-relic).

There are two ways to do the integration depending on the features you want. If you want to incorporate route information into the transaction name, you need to install the integration as a route interceptor so that the matched route will be available:

```kotlin
install(Routing) {
    route("/") {
        setUpNewRelic()
        // other routes, etc...
    }
}
```

If you don't need route information and just the plain request URI or other basic request info is all you need, you can install it as a normal Ktor feature:

```kotlin
install(NewRelic)
```

In either case you can optionally provide a config block to specify a custom `TransactionNamer` implementation.
