import com.jfrog.bintray.gradle.BintrayExtension

plugins {
    kotlin("jvm") version "1.4.0" apply false
    id("com.jfrog.bintray") version "1.8.5" apply false
    id("org.jetbrains.dokka") version "0.10.1" apply false
    id("net.researchgate.release") version "2.8.1"
    id("com.github.ben-manes.versions") version "0.29.0"
    id("org.jmailen.kotlinter") version "3.0.0" apply false
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "org.jmailen.kotlinter")

    repositories {
        jcenter()
    }

    @Suppress("UNUSED_VARIABLE")
    val deps by extra {
        mapOf(
            "ktor" to "1.4.0",
            "junit" to "5.6.2",
            "slf4j" to "1.7.30"
        )
    }

    tasks.named<Test>("test") {
        useJUnitPlatform()
    }
}


subprojects.filter { listOf("ktor-new-relic", "coroutines-new-relic").contains(it.name) }.forEach { project ->
    project.run {
        apply(plugin = "maven-publish")
        apply(plugin = "com.jfrog.bintray")
        apply(plugin = "org.jetbrains.dokka")

        tasks {
            register<Jar>("sourceJar") {
                from(project.the<SourceSetContainer>()["main"].allSource)
                archiveClassifier.set("sources")
            }

            register<Jar>("docJar") {
                from(project.tasks["dokka"])
                archiveClassifier.set("javadoc")
            }
        }

        configure<PublishingExtension> {
            publications {
                register<MavenPublication>("bintray") {
                    from(components["java"])
                    artifact(tasks["sourceJar"])
                    artifact(tasks["docJar"])
                }
            }
        }

        configure<BintrayExtension> {
            user = rootProject.findProperty("bintrayUser")?.toString()
            key = rootProject.findProperty("bintrayApiKey")?.toString()
            setPublications("bintray")

            with(pkg) {
                repo = "maven"
                setLicenses("Copyfree")
                vcsUrl = "https://bitbucket.org/marshallpierce/kotlin-new-relic"
                // use per-project bintray packages because they have different group ids, and only one
                // path per project can be synced
                name = project.name

                with(version) {
                    name = project.version.toString()
                    released = java.util.Date().toString()
                    vcsTag = project.version.toString()
                }
            }
        }
    }
}

tasks {
    // dummy for release plugin
    register("build") {}

    afterReleaseBuild {
        dependsOn(named("bintrayUpload"))
    }
}
